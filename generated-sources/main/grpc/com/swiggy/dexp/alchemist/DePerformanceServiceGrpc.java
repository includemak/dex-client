package com.swiggy.dexp.alchemist;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.1.2)",
    comments = "Source: DePerformance.proto")
public class DePerformanceServiceGrpc {

  private DePerformanceServiceGrpc() {}

  public static final String SERVICE_NAME = "DePerformanceService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.swiggy.dexp.alchemist.DePerformance.DePerformanceRequest,
      com.swiggy.dexp.alchemist.DePerformance.DePerformanceResponse> METHOD_GET_PERFORMANCE_FOR_DE =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "DePerformanceService", "getPerformanceForDe"),
          io.grpc.protobuf.ProtoUtils.marshaller(com.swiggy.dexp.alchemist.DePerformance.DePerformanceRequest.getDefaultInstance()),
          io.grpc.protobuf.ProtoUtils.marshaller(com.swiggy.dexp.alchemist.DePerformance.DePerformanceResponse.getDefaultInstance()));

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DePerformanceServiceStub newStub(io.grpc.Channel channel) {
    return new DePerformanceServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DePerformanceServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DePerformanceServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary and streaming output calls on the service
   */
  public static DePerformanceServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DePerformanceServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class DePerformanceServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getPerformanceForDe(com.swiggy.dexp.alchemist.DePerformance.DePerformanceRequest request,
        io.grpc.stub.StreamObserver<com.swiggy.dexp.alchemist.DePerformance.DePerformanceResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_GET_PERFORMANCE_FOR_DE, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_GET_PERFORMANCE_FOR_DE,
            asyncUnaryCall(
              new MethodHandlers<
                com.swiggy.dexp.alchemist.DePerformance.DePerformanceRequest,
                com.swiggy.dexp.alchemist.DePerformance.DePerformanceResponse>(
                  this, METHODID_GET_PERFORMANCE_FOR_DE)))
          .build();
    }
  }

  /**
   */
  public static final class DePerformanceServiceStub extends io.grpc.stub.AbstractStub<DePerformanceServiceStub> {
    private DePerformanceServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DePerformanceServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DePerformanceServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DePerformanceServiceStub(channel, callOptions);
    }

    /**
     */
    public void getPerformanceForDe(com.swiggy.dexp.alchemist.DePerformance.DePerformanceRequest request,
        io.grpc.stub.StreamObserver<com.swiggy.dexp.alchemist.DePerformance.DePerformanceResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_GET_PERFORMANCE_FOR_DE, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class DePerformanceServiceBlockingStub extends io.grpc.stub.AbstractStub<DePerformanceServiceBlockingStub> {
    private DePerformanceServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DePerformanceServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DePerformanceServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DePerformanceServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.swiggy.dexp.alchemist.DePerformance.DePerformanceResponse getPerformanceForDe(com.swiggy.dexp.alchemist.DePerformance.DePerformanceRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_GET_PERFORMANCE_FOR_DE, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class DePerformanceServiceFutureStub extends io.grpc.stub.AbstractStub<DePerformanceServiceFutureStub> {
    private DePerformanceServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DePerformanceServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DePerformanceServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DePerformanceServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.swiggy.dexp.alchemist.DePerformance.DePerformanceResponse> getPerformanceForDe(
        com.swiggy.dexp.alchemist.DePerformance.DePerformanceRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_GET_PERFORMANCE_FOR_DE, getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_PERFORMANCE_FOR_DE = 0;

  private static class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DePerformanceServiceImplBase serviceImpl;
    private final int methodId;

    public MethodHandlers(DePerformanceServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_PERFORMANCE_FOR_DE:
          serviceImpl.getPerformanceForDe((com.swiggy.dexp.alchemist.DePerformance.DePerformanceRequest) request,
              (io.grpc.stub.StreamObserver<com.swiggy.dexp.alchemist.DePerformance.DePerformanceResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class DePerformanceServiceDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.swiggy.dexp.alchemist.DePerformance.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DePerformanceServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DePerformanceServiceDescriptorSupplier())
              .addMethod(METHOD_GET_PERFORMANCE_FOR_DE)
              .build();
        }
      }
    }
    return result;
  }
}
