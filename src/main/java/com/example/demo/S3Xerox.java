package com.example.demo;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.model.Get;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.LoggerFactory;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class S3Xerox {
    private static Logger log = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    static AWSCredentials credentials = new BasicAWSCredentials(
            "AKIAIU2XOBXS7JPLNZGQ",
            "70l8IukxfKsY1KgaJikN2Vj5gsjdVJYAyd4X50or"
    );
    static AmazonS3 s3Client = AmazonS3ClientBuilder
            .standard()
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .withRegion(Regions.AP_SOUTHEAST_1)
            .build();
    static String bucketName = "swiggy.dexp.aggregated";
    public static void main(String[] args){
        log.setLevel(Level.INFO);
        s3Client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();
        String startDate = "2020-06-16 00:00:00";
        String endDate = "2020-07-20 00:00:00";
        String[] folders = {
                "critical_aggregation_event",
                "delivery--de-tipping--dexp_tipping_events",
                "delivery--dexp-orchestrator--de-non-silent-deliveries",
                "delivery--dexp-orchestrator--de_txn_fraud_assessment",
                "delivery--dexp-orchestrator--foundation_de_ratings",
                "delivery--dexp-orchestrator--on_time_deliveries",
                "delivery--dexp-orchestrator--oneview_agent_actions",
                "delivery--dexp-orchestrator--order_cancel",
                "delivery--dexp-orchestrator--order_status_update"};
        copyPasteDeleteS3Files(startDate, endDate, folders);

    }

    private static void copyPasteDeleteS3Files(String startDate, String endDate, String[] folders) {


        DateTimeFormatter fromFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH");
        DateTimeFormatter quarterFormatter = DateTimeFormatter.ofPattern("mm");
        LocalDateTime start = LocalDateTime.parse(startDate,fromFormatter);
        LocalDateTime end = LocalDateTime.parse(endDate,fromFormatter);
        while(start.isBefore(end)){
            LocalDateTime day = start.minusDays(1);
            String dayString = day.format(dateFormatter);
            System.out.println("processing Day="+dayString);
            processAggregatedDay(dayString, folders);
            if(start.getDayOfWeek().equals(DayOfWeek.MONDAY)){
                LocalDateTime week = start.minusWeeks(1);
                String weekString = week.format(dateFormatter);
                processAggregatedWeek(weekString,folders);
            }
            if(start.getDayOfMonth() == 1){
                LocalDateTime month = start.minusMonths(1);
                String monthString = month.format(dateFormatter);
                processAggregatedMonth(monthString, folders);
            }
            for(int hour =0 ; hour<24; hour++){
                LocalDateTime hourDate = start.minusHours(1);
                String  hourString = hourDate.format(hourFormatter);
                processAggregatedHour(dayString, hourString, folders);
                for(int quarter =0; quarter<4; quarter++){
                    LocalDateTime quarterDate = start.minusMinutes(15);
                    String  quarterString = quarterDate.format(quarterFormatter);
                    processAggregatedQuarter(dayString, hourString, quarterString,folders);
                    start = start.plusMinutes(15);
                }
            }
            start = start.plusDays(1);
        }
    }

    private static void processAggregatedDay(String dayString, String[] folders) {
        for(String folder :folders){
            String prefix = folder+"/DAY/"+dayString;
            //System.out.println("Day Prefix = "+prefix);
            ObjectListing objectListing = s3Client.listObjects(bucketName,prefix);
            for(S3ObjectSummary os : objectListing.getObjectSummaries()) {
                if(os.getKey().contains("SUCCESS")){
                    continue;
                }
                //System.out.println("os.getKey()="+os.getKey());
                copyFile(bucketName,prefix, os.getKey());
                deleteOldFile(bucketName,prefix,os.getKey());
            }
        }
    }

    private static void processAggregatedWeek(String weekString, String[] folders) {
        for(String folder :folders){
            String prefix = folder+"/WEEK/"+weekString;
            //System.out.println("Week Prefix = "+prefix);
            ObjectListing objectListing = s3Client.listObjects(bucketName,prefix);
            for(S3ObjectSummary os : objectListing.getObjectSummaries()) {
                if(os.getKey().contains("SUCCESS")){
                    continue;
                }
                //System.out.println("os.getKey()="+os.getKey());
                copyFile(bucketName,prefix, os.getKey());
                deleteOldFile(bucketName,prefix,os.getKey());
            }
        }
    }

    private static void processAggregatedMonth(String monthString, String[] folders) {
        for(String folder :folders){
            String prefix = folder+"/MONTH/"+monthString;
            System.out.println("Month Prefix = "+prefix);
            ObjectListing objectListing = s3Client.listObjects(bucketName,prefix);
            for(S3ObjectSummary os : objectListing.getObjectSummaries()) {
                if(os.getKey().contains("SUCCESS")){
                    continue;
                }
                System.out.println("os.getKey()="+os.getKey());
                copyFile(bucketName,prefix, os.getKey());
                deleteOldFile(bucketName,prefix,os.getKey());
            }
        }
    }

    private static void processAggregatedHour(String dayString,String hourString, String[] folders) {
        for(String folder :folders){
            String prefix = folder+"/HOUR/"+dayString+"/"+hourString;
            //System.out.println("Hour Prefix = "+prefix);
            ObjectListing objectListing = s3Client.listObjects(bucketName,prefix);
            for(S3ObjectSummary os : objectListing.getObjectSummaries()) {
                if(os.getKey().contains("SUCCESS")){
                    continue;
                }
                //System.out.println("os.getKey()="+os.getKey());
                copyFile(bucketName,prefix, os.getKey());
                deleteOldFile(bucketName,prefix,os.getKey());
            }
        }
    }

    private static void processAggregatedQuarter(String dayString,String hourString, String quarterString, String[] folders) {
        String prefix = "delivery--dexp-orchestrator--order_cancel"+"/QUARTER/"+dayString+"/"+hourString+"/"+quarterString;
        //System.out.println("QUARTER Prefix = "+prefix);
        ObjectListing objectListing = s3Client.listObjects(bucketName,prefix);
        for(S3ObjectSummary os : objectListing.getObjectSummaries()) {
            if(os.getKey().contains("SUCCESS")){
                continue;
            }
            //System.out.println("os.getKey()="+os.getKey());
            copyFile(bucketName,prefix, os.getKey());
            deleteOldFile(bucketName,prefix,os.getKey());
        }
    }

    private static void deleteOldFile(String bucketName, String prefix, String key) {
        s3Client.deleteObject(bucketName,key);
    }

    private static void copyFile(String bucketName, String prefix, String fileName) {
        if(fileName.contains("part")){
            String dest = fileName.replaceAll("part","copy");
            CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, fileName, bucketName, dest);
            s3Client.copyObject(copyObjRequest);
        }
    }
}
