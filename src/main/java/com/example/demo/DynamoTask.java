package com.example.demo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import org.springframework.scheduling.annotation.Async;

import java.security.PrivilegedAction;
import java.security.PrivilegedExceptionAction;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DynamoTask implements Runnable {

    private AmazonDynamoDBAsync dynamoDBAsync;
    private String sortKeyVal;
    private String partitionKeyVal;
    private Integer deliveredCount;
    private Integer rejectedCount;
    private Integer ratingSum;
    private Integer ratingCount;
    private Integer loggedInMinutes;
    private String  tableName;

    public DynamoTask(AmazonDynamoDBAsync dynamoDBAsync, String table, String sortKeyVal, String partitionKeyVal, Integer deliveredCount, Integer rejectedCount, Integer ratingSum, Integer ratingCount,Integer loggedInMinutes) {
        this.dynamoDBAsync = dynamoDBAsync;
        this.tableName = table;
        this.sortKeyVal = sortKeyVal;
        this.partitionKeyVal = partitionKeyVal;
        this.deliveredCount = deliveredCount;
        this.rejectedCount = rejectedCount;
        this.ratingSum = ratingSum;
        this.ratingCount = ratingCount;
        this.loggedInMinutes = loggedInMinutes;
    }

    @Override
    public void run() {
        try {
            HashMap<String, AttributeValue> key = new HashMap<String, AttributeValue>();
            key.put("main_partition_key", new AttributeValue().withS(partitionKeyVal));
            key.put("main_sort_key", new AttributeValue().withS(sortKeyVal));

            Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
            expressionAttributeValues.put(":doc",new AttributeValue().withN(String.valueOf(deliveredCount)));
            expressionAttributeValues.put(":roc", new AttributeValue().withN(String.valueOf(rejectedCount)));
            expressionAttributeValues.put(":rs", new AttributeValue().withN(String.valueOf(ratingSum)));
            expressionAttributeValues.put(":rc", new AttributeValue().withN(String.valueOf(ratingCount)));
            expressionAttributeValues.put(":lim", new AttributeValue().withN(String.valueOf(loggedInMinutes)));

            UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                    .withTableName(tableName)
                    .withKey(key)
                    .withUpdateExpression("set delivered_order_count = :doc, rejected_order_count=:roc, rating_sum=:rs, rating_count=:rc, logged_in_minutes=:lim")
                    .withExpressionAttributeValues(expressionAttributeValues)
                    .withReturnValues(ReturnValue.NONE);
            dynamoDBAsync.updateItemAsync(updateItemRequest);
        }
        catch (Exception e) {
            System.err.println("Create items failed.");
            System.err.println(e.getMessage());
        }
    }

}
