package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.net.ssl.SSLException;
import java.util.concurrent.ExecutionException;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) throws InterruptedException, ExecutionException, SSLException {
		SpringApplication.run(DemoApplication.class, args);
		//ThreadedDynamoTest.main(args);
		GrpcClientWithSSL.main(args);
	}

}
