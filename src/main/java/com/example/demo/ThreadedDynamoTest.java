package com.example.demo;

import ch.qos.logback.classic.Level;
import com.amazon.dax.client.dynamodbv2.AmazonDaxAsyncClient;
import com.amazon.dax.client.dynamodbv2.AmazonDaxAsyncClientBuilder;
import com.amazon.dax.client.dynamodbv2.AmazonDaxClientBuilder;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AdvancedConfig;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsync;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBAsyncClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.util.StringUtils;


import java.util.*;
import java.util.concurrent.*;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.Logger;

@SuppressWarnings("ALL")
public class ThreadedDynamoTest {
    private static Logger log = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    public static void main(String args[]) throws InterruptedException, ExecutionException {
        log.setLevel(Level.INFO);
        readItem();
    }

    private static void writeNDaysDataOfMDEs(int n, int m) {
        AmazonDynamoDBAsync dynamoDBAsync = amazonDynamoDB();
        Queue<Future<UpdateItemResult>> updateItemResults = new LinkedList<>();
        String tableName = "delivery-dexp-de-info";
        long start = System.currentTimeMillis();
        long per55DE = System.currentTimeMillis();
        int weekStartDate = 6;
        for(int deId=0;deId<m;deId++){
            String weekPK = "WEEK#"+deId;
            String weekSK = "WEEK#2019-10-0"+weekStartDate+" 00:00:00";
            updateItemResults.add(updateItemForDEWithTimeStamp(dynamoDBAsync,weekPK,weekSK));
            for(int j=0;j<n;j++){
                int date = weekStartDate+j;
                String dayPK = "DAY#"+deId;
                String dayString = "";
                if(date < 10){
                    dayString +="0"+date;
                }else{
                    dayString +=+date;
                }
                String daySK = "DAY#2019-10-"+dayString+" 00:00:00";
                updateItemResults.add(updateItemForDEWithTimeStamp(dynamoDBAsync,dayPK,daySK));
                for(int hour =0; hour<24;hour++){
                    String hourPK = "HOUR#"+deId;
                    String hourString = "2019-10-"+dayString;
                    if(hour < 10){
                        hourString += " 0"+hour;
                    }else{
                        hourString += " "+hour;
                    }
                    String hourSK = "HOUR#"+hourString+":00:00";
                    updateItemResults.add(updateItemForDEWithTimeStamp(dynamoDBAsync,hourPK,hourSK));
                    for(int quarter=0;quarter<4;quarter++){
                        String quarterPK = "QUARTER#"+deId;
                        String quarterString = "";
                        if(quarter*15 < 10){
                            quarterString += "0"+(quarter*15);
                        }else{
                            quarterString += +(quarter*15);
                        }
                        String quarterSK = "QUARTER#2019-10-"+dayString+" "+hourString+":"+quarterString+":00";
                        updateItemResults.add(updateItemForDEWithTimeStamp(dynamoDBAsync,quarterPK,quarterSK));
                    }
                }
            }
            if(deId%55==0){
                System.out.println("time to update 55 DE="+(System.currentTimeMillis()-per55DE));
                per55DE = System.currentTimeMillis();
            }
        }
        System.out.println("all processed in "+(System.currentTimeMillis()-start));
        long counter = 0l;
        long time = System.currentTimeMillis();
        long overAllTime = System.currentTimeMillis();
        while (!updateItemResults.isEmpty()){
            Future<UpdateItemResult> top =updateItemResults.poll();
            if(top.isCancelled() || top.isDone()){
                counter++;
                if(counter%55 == 0){
                    System.out.println("55 update time ="+(System.currentTimeMillis()-time));
                    time = System.currentTimeMillis();
                }
            }else{
                ((LinkedList<Future<UpdateItemResult>>) updateItemResults).addFirst(top);
            }
        }
        dynamoDBAsync.shutdown();
        System.out.println("time taken to finish task ={}"+(System.currentTimeMillis() - overAllTime));
    }

    private static Future<UpdateItemResult> updateItemForDEWithTimeStamp(AmazonDynamoDBAsync dynamoDBAsync, String pk, String sk) {
        System.out.println("pk ="+pk +", sk= "+sk);
        String tableName = "delivery-dexp-de-info";
        Integer deliveredCount = 5;
        Integer rejectedCount = 5;
        Integer ratingSum = 5;
        Integer ratingCount = 5;
        Integer loggedInMinutes= 5;
        HashMap<String, AttributeValue> key = new HashMap<String, AttributeValue>();
        key.put("main_partition_key", new AttributeValue().withS(pk));
        key.put("main_sort_key", new AttributeValue().withS(sk));
        Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
        expressionAttributeValues.put(":doc",new AttributeValue().withN(String.valueOf(deliveredCount)));
        expressionAttributeValues.put(":roc", new AttributeValue().withN(String.valueOf(rejectedCount)));
        expressionAttributeValues.put(":rs", new AttributeValue().withN(String.valueOf(ratingSum)));
        expressionAttributeValues.put(":rc", new AttributeValue().withN(String.valueOf(ratingCount)));
        expressionAttributeValues.put(":lim", new AttributeValue().withN(String.valueOf(loggedInMinutes)));
        UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                .withTableName(tableName)
                .withKey(key)
                .withUpdateExpression("set delivered_order_count = :doc, rejected_order_count=:roc, rating_sum=:rs, rating_count=:rc, logged_in_minutes=:lim")
                .withExpressionAttributeValues(expressionAttributeValues)
                .withReturnValues(ReturnValue.NONE);
        Future<UpdateItemResult> res = dynamoDBAsync.updateItemAsync(updateItemRequest);
        return res;
    }

    private static void readItem() throws ExecutionException, InterruptedException {
        AmazonDynamoDBAsync dynamoDBAsync = amazonDynamoDB();
        String tableName = "delivery-dexp-de-info";
        long start = System.currentTimeMillis();

        System.out.println("started at "+start);
        long perDETime = System.currentTimeMillis();
        for(int j=0;j<1;j++){
            for(int i=0;i<1;i++){
                BatchGetItemRequest batchGetItemRequest = getBatchGetItemRequest(1,i);
                Future<BatchGetItemResult> batchGetItemResultFuture = dynamoDBAsync.batchGetItemAsync(batchGetItemRequest);
                BatchGetItemResult result = batchGetItemResultFuture.get();
                for(String table : result.getResponses().keySet()){
                    for(Map<String, AttributeValue> map : result.getResponses().get(table)){
                        for(String str : map.keySet()){
                            System.out.println("str= "+str+" , value="+map.get(str)+" atv ="+map.get(str).getNS());
                        }
                    }
                }
            }
            System.out.println("per DE with id="+j+" Time ="+(System.currentTimeMillis()-perDETime));
            perDETime = System.currentTimeMillis();
        }
        System.out.println("finished in "+(System.currentTimeMillis() - start));
        System.out.println("consumed capacity ");
        dynamoDBAsync.shutdown();

        /*System.out.println("############################# will read from dax now #############################");
        String daxURL = "delivery-dexp-de.fzgzpa.clustercfg.dax.apse1.cache.amazonaws.com:8111";
        AmazonDynamoDBAsync dynamoDBAsyncDax = getDaxClient(daxURL);
        start = System.currentTimeMillis();
        System.out.println("started at "+start);
        perDETime = 0;
        for(int j=0;j<1;j++){
            for(int i=0;i<2;i++){
                BatchGetItemRequest batchGetItemRequest = getBatchGetItemRequest(1,i);
                Future<BatchGetItemResult> batchGetItemResultFuture = dynamoDBAsync.batchGetItemAsync(batchGetItemRequest);
                BatchGetItemResult result = batchGetItemResultFuture.get();
            }
            System.out.println("per DE with id="+j+" Time ="+(System.currentTimeMillis()-perDETime));
            perDETime = System.currentTimeMillis();
        }
        System.out.println("finished in "+(System.currentTimeMillis() - start));
        dynamoDBAsync.shutdown();*/
    }

    private static BatchGetItemRequest getBatchGetItemRequest(int j, int sortMultiplier) {
        String tableName = "delivery-dexp-de-info";
        AmazonDynamoDBAsync dynamoDBAsync = amazonDynamoDB();
        Map<String,KeysAndAttributes> requestItems = new HashMap<>();
        List<Map<String,AttributeValue>> keys = new ArrayList<>();
        String partitionKeyVal = "QUARTER#"+j;
        for(int i=(sortMultiplier*100); i<((sortMultiplier*100)+99); i++){
            Map<String,AttributeValue> keyAttributes = new HashMap<>();
            keyAttributes.put("main_partition_key",new AttributeValue().withS(partitionKeyVal));
            String sortKeyVal = "QUARTER#2019-01-"+i+" 00:00:00";
            keyAttributes.put("main_sort_key",new AttributeValue().withS(sortKeyVal));
            keys.add(keyAttributes);
        }
        System.out.println("total keys values ="+keys.size());
        requestItems.put(tableName,new KeysAndAttributes()
                .withKeys(keys)
                .withAttributesToGet("delivered_order_count", "rejected_order_count", "rating_sum","rating_count", "logged_in_minutes"));
        BatchGetItemRequest batchGetItemRequest = new BatchGetItemRequest();
        batchGetItemRequest.setRequestItems(requestItems);
        return batchGetItemRequest;
    }

    private static void updateItem(){
        AmazonDynamoDBAsync dynamoDBAsync = amazonDynamoDB();
        String tableName = "delivery-dexp-de-info";
        long start = System.currentTimeMillis();
        System.out.println("data ingetion started at ={}"+start);
        Queue<Future<UpdateItemResult>> updateItemResults = new LinkedList<>();
        for (int i=0;i<200000;i++) {
            String partitionValue = "QUARTER#"+i;
            for(int j=0;j<200;j++){
                String sortKeyVal = "QUARTER#2019-01-"+j+" 00:00:00";
                Integer deliveredCount = i;
                Integer rejectedCount = i;
                Integer ratingSum = i;
                Integer ratingCount = i;
                Integer loggedInMinutes= i;
                HashMap<String, AttributeValue> key = new HashMap<String, AttributeValue>();
                key.put("main_partition_key", new AttributeValue().withS(partitionValue));
                key.put("main_sort_key", new AttributeValue().withS(sortKeyVal));
                Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
                expressionAttributeValues.put(":doc",new AttributeValue().withN(String.valueOf(deliveredCount)));
                expressionAttributeValues.put(":roc", new AttributeValue().withN(String.valueOf(rejectedCount)));
                expressionAttributeValues.put(":rs", new AttributeValue().withN(String.valueOf(ratingSum)));
                expressionAttributeValues.put(":rc", new AttributeValue().withN(String.valueOf(ratingCount)));
                expressionAttributeValues.put(":lim", new AttributeValue().withN(String.valueOf(loggedInMinutes)));
                UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                        .withTableName(tableName)
                        .withKey(key)
                        .withUpdateExpression("set delivered_order_count = :doc, rejected_order_count=:roc, rating_sum=:rs, rating_count=:rc, logged_in_minutes=:lim")
                        .withExpressionAttributeValues(expressionAttributeValues)
                        .withReturnValues(ReturnValue.NONE);
                Future<UpdateItemResult> res = dynamoDBAsync.updateItemAsync(updateItemRequest);
                //updateItemResults.add(res);
            }
            System.out.println("done with de="+i);
        }

        //System.out.println("time taken to send dynamo task ={}"+(System.currentTimeMillis() - start));
        long counter = 0l;
        long time = System.currentTimeMillis();
        long overAllTime = System.currentTimeMillis();
        while (!updateItemResults.isEmpty()){
            Future<UpdateItemResult> top =updateItemResults.poll();
            if(top.isCancelled() || top.isDone()){
                counter++;
                if(counter%1000 == 0){
                    System.out.println("1000 processing time ="+(System.currentTimeMillis()-time));
                    time = System.currentTimeMillis();
                }
            }else{
                ((LinkedList<Future<UpdateItemResult>>) updateItemResults).addFirst(top);
            }
        }
        dynamoDBAsync.shutdown();
        System.out.println("time taken to finish task ={}"+(System.currentTimeMillis() - overAllTime));
    }
    private static void createNewTable(DynamoDB db, String tableName) {
        ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
        attributeDefinitions.add(new AttributeDefinition().withAttributeName("main_partition_key").withAttributeType("S"));
        attributeDefinitions.add(new AttributeDefinition().withAttributeName("main_sort_key").withAttributeType("S"));

        // Key schema for table
        ArrayList<KeySchemaElement> tableKeySchema = new ArrayList<KeySchemaElement>();
        tableKeySchema.add(new KeySchemaElement().withAttributeName("main_partition_key").withKeyType(KeyType.HASH)); // Partitionkey
        tableKeySchema.add(new KeySchemaElement().withAttributeName("main_sort_key").withKeyType(KeyType.RANGE)); // Sortkey

        // Initial provisioned throughput settings
        ProvisionedThroughput pt = new ProvisionedThroughput().withReadCapacityUnits(100L).withWriteCapacityUnits(666l);
        CreateTableRequest createTableRequest = new CreateTableRequest().withTableName(tableName)
                .withProvisionedThroughput(pt)
                .withAttributeDefinitions(attributeDefinitions).withKeySchema(tableKeySchema);

        System.out.println("Creating table " + tableName + "...");
        try {
            db.createTable(createTableRequest);
        }catch (ResourceInUseException e) {
            log.info("No need to create table table is already created");
        }

        System.out.println("Waiting for " + tableName + " to become ACTIVE...");
        try {
            Table table = db.getTable(tableName);
            table.waitForActive();
        }
        catch (InterruptedException e) {
            log.error("Error in creating table",e);
        }
        log.info("created table for entity "+tableName);
    }

    private static DynamoDB getDynamoDB() {
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB());
        return dynamoDB;
    }

    private static AmazonDynamoDBAsync getDaxClient(String daxEndpoint) {
        System.out.println("Creating a DAX client with cluster endpoint " + daxEndpoint);
        AmazonDaxAsyncClientBuilder daxClientBuilder = AmazonDaxAsyncClientBuilder.standard();
        Regions region = Regions.fromName("ap-southeast-1");
        daxClientBuilder.withRegion(region.getName()).withEndpointConfiguration(daxEndpoint);
        AmazonDynamoDBAsync client = daxClientBuilder.build();
        return client;
    }

    public static AmazonDynamoDBAsync amazonDynamoDB() {

        String accessKey = "AKIATDLCJBP7Q3A6AFFR";
        String secretKey = "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";
        String endpointURL = "http://dynamodb.ap-southeast-1.amazonaws.com";
        String regionName = "ap-southeast-1";
        Regions region = Regions.fromName(regionName);
        ClientConfiguration config = getConfig();
        AmazonDynamoDBAsync builder = AmazonDynamoDBAsyncClientBuilder.standard()
                .withCredentials(
                        new AWSCredentialsProvider() {
                            @Override
                            public AWSCredentials getCredentials() {
                                return new BasicAWSCredentials(accessKey, secretKey);
                            }
                            @Override
                            public void refresh() {

                            }
                        }
                )
                .withClientConfiguration(config)
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpointURL,
                        region.getName()))
                //.withExecutorFactory(() -> Executors.newFixedThreadPool(400))
                .build();
        return builder;
    }

    private static ClientConfiguration getConfig() {
        int connectionTimeout = ClientConfiguration.DEFAULT_CONNECTION_TIMEOUT;
        int maxConnections = ClientConfiguration.DEFAULT_MAX_CONNECTIONS;
        int maxErrorRetry = -1;
        boolean throttleRetries = ClientConfiguration.DEFAULT_THROTTLE_RETRIES;
        int socketTimeout = ClientConfiguration.DEFAULT_SOCKET_TIMEOUT;
        int requestTimeout = ClientConfiguration.DEFAULT_REQUEST_TIMEOUT;
        int clientExecutionTimeout = ClientConfiguration.DEFAULT_CLIENT_EXECUTION_TIMEOUT;
        long connectionTTL = ClientConfiguration.DEFAULT_CONNECTION_TTL;
        long connectionMaxIdleMillis = ClientConfiguration.DEFAULT_CONNECTION_MAX_IDLE_MILLIS;
        int validateAfterInactivityMillis = ClientConfiguration.DEFAULT_VALIDATE_AFTER_INACTIVITY_MILLIS;
        int maxConsecutiveRetriesBeforeThrottling = ClientConfiguration.DEFAULT_MAX_CONSECUTIVE_RETRIES_BEFORE_THROTTLING;
        return new ClientConfiguration()
                .withConnectionTimeout(connectionTimeout)
                .withSocketTimeout(socketTimeout)
                .withClientExecutionTimeout(clientExecutionTimeout)
                .withConnectionMaxIdleMillis(connectionMaxIdleMillis)
                .withMaxConnections(maxConnections)
                .withThrottledRetries(throttleRetries)
                .withRequestTimeout(requestTimeout)
                .withConnectionTTL(connectionTTL)
                .withValidateAfterInactivityMillis(validateAfterInactivityMillis)
                .withMaxConsecutiveRetriesBeforeThrottling(maxConsecutiveRetriesBeforeThrottling);
    }
}
