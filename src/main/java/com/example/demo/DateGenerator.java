package com.example.demo;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class DateGenerator {
    public static void main(String args[]) throws IOException {
        //putBatchCols();
        putRealTimeCols();
    }

    private static void putRealTimeCols() throws IOException {
        DateTime start = DateTime.now().withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter formatter2 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        String[] cols = getCols();
        int deId = 23;
        BufferedWriter writer = new BufferedWriter(new FileWriter("real_time_dynamo_backup.txt"));

        String colsString ="";
        for(String str: cols){
            colsString += "\""+str+"_00\": {\"N\": \"1\"},";
            colsString += "\""+str+"_15\": {\"N\": \"1\"},";
            colsString += "\""+str+"_30\": {\"N\": \"1\"},";
            colsString += "\""+str+"_45\": {\"N\": \"1\"},";
        }
        colsString=colsString.substring(0,colsString.length()-1);
        colsString += "";

        for(int hour=0;hour <24;hour++){
            String hourFolder = formatter2.print(start);
            String hourRow = "aws dynamodb put-item     --table-name delivery-dexp-de-info-real-time  --item '{\"main_partition_key\": {\"S\": \""+"RTHOUR#"+deId+"\"}, \"main_sort_key\": {\"S\": \"RTHOUR#"+hourFolder+"\"},"+colsString+"}' --endpoint-url http://localhost:8000";
            writer.write(hourRow+System.lineSeparator());
            start = start.plusHours(1);
        }
        writer.close();
    }

    private static void putBatchCols() throws IOException {
        DateTime start = DateTime.now().withDate(2019,12,22).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter formatter2 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        String[] cols = getCols();
        int deId = 23;
        BufferedWriter writer = new BufferedWriter(new FileWriter("dynamo_backup.txt"));



        for(int i=0;i<3;i++){
            String dayFolder = formatter.print(start);
            //System.out.println("DAY#"+deId+""+"\tDAY#"+dayFolder);
            String colsString ="";
            for(String str: cols){
                colsString += "\""+str+"\": {\"N\": \"1\"},";
            }
            colsString=colsString.substring(0,colsString.length()-1);
            colsString += "";
            String dayRow = "aws dynamodb put-item     --table-name delivery-dexp-de-info  --item '{\"main_partition_key\": {\"S\": \""+"DAY#"+deId+"\"}, \"main_sort_key\": {\"S\": \"DAY#"+dayFolder+"\"},"+colsString+"}' --endpoint-url http://localhost:8000";
            writer.write(dayRow+System.lineSeparator());
            for(int hour=0;hour <24;hour++){
                String hourFolder = formatter2.print(start);
                String hourRow = "aws dynamodb put-item     --table-name delivery-dexp-de-info  --item '{\"main_partition_key\": {\"S\": \""+"HOUR#"+deId+"\"}, \"main_sort_key\": {\"S\": \"HOUR#"+hourFolder+"\"},"+colsString+"}' --endpoint-url http://localhost:8000";
                writer.write(hourRow+System.lineSeparator());
                for(int min =0; min<60;min=min+15){
                    String minFolder = formatter2.print(start);
                    String quarterRow = "aws dynamodb put-item     --table-name delivery-dexp-de-info  --item '{\"main_partition_key\": {\"S\": \""+"QUARTER#"+deId+"\"}, \"main_sort_key\": {\"S\": \"QUARTER#"+minFolder+"\"},"+colsString+"}' --endpoint-url http://localhost:8000";
                    writer.write(quarterRow+System.lineSeparator());
                    start = start.plusMinutes(15);
                }
            }
        }
        writer.close();
    }

    private static String[] getCols() {
        String[] cols = {"ppc_bike_issue","ppc_dnr","ppc_accident","ppc_blackzone","ppc_unable_to_deliver","de_driven_ppc","de_rude_behavior","rating_too_many_calls","rating_delayed_delivery","cc_made_customer_unsafe","cc_de_photo_mismatch","cc_customer_pickup","cc_rude_behavior","de_rejects_post_conf","de_rejects_pre_conf","de_fraud_can_offline_delivery","de_fraud_so_de_self_order","de_fraud_so_de_rx_collusion","de_fraud_igcc_claimed_mdi","delivered_order_count","rejected_order_count","logged_in_minutes","rating_sum","rating_count"};
        return cols;
    }
}
