package com.example.demo;

import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;

import javax.jms.JMSException;
import javax.jms.Session;
import java.util.HashMap;

public class SQSTesting {

    public static void main(String[] args){
        try {
            triggerSQSNotifications();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    private static void triggerSQSNotifications() throws JMSException {
        String a = "AKIAVIKMSX7323DOBUOX";
        String s = "tJmZLe1VoGBRXD6WGaNTrdcTkQK7A8W1eGKhqXzD";
        String e = "https://sqs.ap-southeast-1.amazonaws.com/361474736119/delivery-dex-aggregation-queue";
        SQSConnectionFactory connectionFactory = SQSConnectionFactory.builder().withAWSCredentialsProvider(new AWSCredentialsProvider() {
            public AWSCredentials
            getCredentials() {
                return new BasicAWSCredentials(a, s);
            }
            public void refresh() {
            }
        }).withEndpoint(e).build();
        SQSConnection connection = connectionFactory.createConnection();
        Session session = connection.createSession(false, javax.jms.Session.CLIENT_ACKNOWLEDGE);
        String queue = "delivery-dex-aggregation-queue";
        AmazonSQS sqslClient = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(a, s)))
                .withRegion(Regions.fromName("ap-southeast-1"))
                .build();
        String msgBody = "{\"Records\":[{\"eventVersion\":\"2.1\",\"eventSource\":\"aws:s3\",\"awsRegion\":\"ap-southeast-1\"," +
                "\"eventTime\":\"2020-07-10T00:40:48.537Z\",\"eventName\":\"ObjectCreated:Copy\"," +
                "\"userIdentity\":{\"principalId\":\"AWS:AIDAIS42RVLG66FW4TCBU\"},\"requestParameters\":{\"sourceIPAddress\":\"172.31.88.97\"}," +
                "\"responseElements\":{\"x-amz-request-id\":\"0SFX4P8S3V6G8MDP\"," +
                "\"x-amz-id-2\":\"dlYOjfyF4qz0bif2KhQYVVhGRlKgi6Mk/lP6pl1EzgBA+PN9oUomLVu7bD6zgKL2z+xR5IRpxDhpYdA3mf3r9bbadpDpTZ3/\"}," +
                "\"s3\":{\"s3SchemaVersion\":\"1.0\",\"configurationId\":\"delivery-sqs-queue\"," +
                "\"bucket\":{\"name\":\"swiggy.dexp.aggregated\",\"ownerIdentity\":{\"principalId\":\"A316YWZINFH5F8\"}," +
                "\"arn\":\"arn:aws:s3:::swiggy.dexp.aggregated\"},\"object\":{\"key\":\"\n" +
                "delivery--de-tipping--dexp_tipping_events/DAY/2020-07-10/part-00000-982a2b87-6f53-4167-8ddf-16e7695601bf-c000.json\",\"size\":186," +
                "\"eTag\":\"d41d8cd98f00b204e9800998ecf8427e\",\"versionId\":\"5frR2Ty093HmOb4Vs4nFXI0WjwhHiypP\",\"sequencer\":\"005F163992E425F84C\"}}}]}";
        for (int i = 1; i <= 1; i++) {
            int finalI = i;
            SendMessageRequest sendMessageRequest = new SendMessageRequest(queue, msgBody)
                    .withMessageAttributes(new HashMap() {{
                        put(String.valueOf(finalI), new MessageAttributeValue()
                                .withStringValue(String.valueOf(finalI))
                                .withDataType("String"));
                    }});
            SendMessageResult res = sqslClient.sendMessage(sendMessageRequest);
            System.out.println("result========="+res);
        }
    }
}
