package com.example.demo;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.util.StringUtils;
import org.slf4j.Logger;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class DeliveredRejectCount {

    static AmazonDynamoDB client = amazonDynamoDB();
    private static long timestamp = 0;
    private static long printed = 0;
    public static  AmazonDynamoDB amazonDynamoDB() {

        String accessKey = "AKIATDLCJBP7Q3A6AFFR";
        String secretKey = "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";
        String endpointURL = "http://dynamodb.ap-southeast-1.amazonaws.com";
        String regionName = "ap-southeast-1";
        Regions region = Regions.fromName(regionName);
        ClientConfiguration config = getConfig();
        AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(
                        new AWSCredentialsProvider() {
                            @Override
                            public AWSCredentials getCredentials() {
                                return new BasicAWSCredentials(accessKey, secretKey);
                            }
                            @Override
                            public void refresh() {

                            }
                        }
                )
                .withClientConfiguration(config);

        /**
         * Either of "Region" or "EndpointURL and Region (signingRegion)" is needed
         */
        if (!StringUtils.isNullOrEmpty(endpointURL)) {
            AwsClientBuilder.EndpointConfiguration endpointConfiguration = new AwsClientBuilder.EndpointConfiguration(endpointURL,
                    region.getName());
            builder.withEndpointConfiguration(endpointConfiguration);
        } else {
            builder.withRegion(region);
        }
        return builder.build();
    }

    static DynamoDB dynamoDB = new DynamoDB(client);
    static String tableName = "delivery-dexp-de-info";
    static String hashKeyName = "main_partition_key";
    static String rangeKeyName = "main_sort_key";

    public static void main(String args[]) throws IOException {
        /*Table table = dynamoDB.getTable(tableName);
        try {
            Item item = new Item().withPrimaryKey(hashKeyName, "DAY#"+12345,rangeKeyName,"DAY#12345")
                    .withNumber("delivered_order_count", 22);
            table.putItem(item);
        }
        catch (Exception e) {
            System.err.println("Create items failed.");
            System.err.println(e.getMessage());
        }*/
       /* createDailyDeliveredCountItem();
        System.out.println("daily delivered count inserted");
        createDailyRejectCountItem();
        System.out.println("daily reject count inserted");*/
        /*Table table = dynamoDB.getTable(tableName);
        try {
            Item item = new Item().withPrimaryKey(hashKeyName, "DAY#"+1,rangeKeyName,"DAY#"+1212)
                    .withNumber("delivered_order_count", 1);
            table.putItem(item);
        }
        catch (Exception e) {
            System.err.println("Create items failed.");
            System.err.println(e.getMessage());
        }*/
        //deleteItems();
        //deleteAllItemFromTable();
        timestamp = System.currentTimeMillis();
        createDailyDeliveredCountItem();
        System.out.println("done");
    }

    private static void deleteAllItemFromTable(){
        Table table = dynamoDB.getTable(tableName);
        //ItemCollection<ScanOutcome> deleteoutcome = table.scan();
        //Iterator<Item> iterator = deleteoutcome.iterator();

        /*while (iterator.hasNext()) {
            Object str = iterator.next().get("main_partition_key");
            System.out.println("deleting ="+str);
            table.deleteItem(hashKeyName , str);
        }*/

        //May be we can make it look generic by reading key schema first as below
        String strPartitionKey = null;
        String strSortKey = null;
        TableDescription description =  table.describe();
        List<KeySchemaElement> schema =  description.getKeySchema();
        for (KeySchemaElement element: schema) {
            if (element.getKeyType().equalsIgnoreCase("HASH"))
                strPartitionKey = element.getAttributeName();
            if (element.getKeyType().equalsIgnoreCase("RANGE"))
                strSortKey = element.getAttributeName();
        }

        ItemCollection<ScanOutcome> deleteoutcome = table.scan();
        Iterator<Item> iterator = deleteoutcome.iterator();
        int i=0;
        while (iterator.hasNext()) {
            System.err.println("#############################################=== "+(i++)+"###########################################################");
            Item next = iterator.next();
            //if (strSortKey == null && strPartitionKey != null)
            //    table.deleteItem(strPartitionKey , next.get(strPartitionKey));
            //else if (strPartitionKey != null && strSortKey != null)
                table.deleteItem(strPartitionKey , next.get(strPartitionKey), strSortKey, next.get(strSortKey));
        }
    }
    private static void deleteItems() {
        String dayTimeStamp="2019-10-01 00:00:00";
        Table table = dynamoDB.getTable(tableName);
        String file = "src/main/resources/s3files/day/delivered_order_count/2019-09-20.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
                String[] data = line.split(cvsSplitBy);
                String deId = data[0];
                table.deleteItem("main_partition_key","DAY#"+deId, "main_sort_key","DAY#"+dayTimeStamp);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void createDailyRejectCountItem() {
        String dayTimeStamp="2019-09-20";
        String file = "src/main/resources/s3files.day.rejected_order_count/2019-09-20.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
                String[] data = line.split(cvsSplitBy);
                String deId = data[0];
                String rejectedCount = data[1];
                putRejctedOrderItem(deId,dayTimeStamp,Integer.parseInt(rejectedCount));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void putRejctedOrderItem(String deId, String dayTimeStamp, int rejectOrderCount) {
        Table table = dynamoDB.getTable(tableName);

        try {

            UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey(hashKeyName, "DAY#"+deId,rangeKeyName,"DAY#"+dayTimeStamp)
                    .withUpdateExpression("set #na = :val1").withNameMap(new NameMap().with("#na", "reject_order_count"))
                    .withValueMap(new ValueMap().withNumber(":val1", rejectOrderCount)).withReturnValues(ReturnValue.ALL_NEW);
            UpdateItemOutcome outcome = table.updateItem(updateItemSpec);

            // Check the response.
            System.out.println("Printing item after adding new attribute...");
            System.out.println(outcome.getItem().toJSONPretty());

        }
        catch (Exception e) {
            System.err.println("Failed to add new attribute in " + tableName);
            System.err.println(e.getMessage());
        }
    }

    private static void createDailyDeliveredCountItem() throws IOException {
        String dayTimeStamp="2019-09-20";
        String file = "src/main/resources/s3files/day/delivered_order_count/2019-09-20.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        long prev = System.currentTimeMillis();
        try {
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
            String[] data = line.split(cvsSplitBy);
                String deId = data[0];
                String deliveredCount = data[1];
                putItem(deId,dayTimeStamp,Integer.parseInt(deliveredCount));
                printed++;
                if(timestamp+1000 <= System.currentTimeMillis()){
                    timestamp = timestamp+1000;
                    System.out.println("################yahoo ="+printed);
                }
                System.out.println("execution time per upsert"+(System.currentTimeMillis() - prev));
                prev = System.currentTimeMillis();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void putItem(String deId, String timeStamp, Integer deliveredOrderCount){
        Table table = dynamoDB.getTable(tableName);
        try {
            Item item = new Item().withPrimaryKey(hashKeyName, "DAY#"+deId,rangeKeyName,"DAY#"+timeStamp)
                    .withNumber("delivered_order_count", deliveredOrderCount);
            table.putItem(item);
            System.out.println("##########################################################puting item##########################################");
            System.out.println("#########################################################date = "+new Date()+"##########################################");
        }
        catch (Exception e) {
            System.err.println("Create items failed.");
            System.err.println(e.getMessage());
        }
    }

    private static ClientConfiguration getConfig() {
         int connectionTimeout = ClientConfiguration.DEFAULT_CONNECTION_TIMEOUT;
         int maxConnections = ClientConfiguration.DEFAULT_MAX_CONNECTIONS;
         int maxErrorRetry = -1;
         boolean throttleRetries = ClientConfiguration.DEFAULT_THROTTLE_RETRIES;
         int socketTimeout = ClientConfiguration.DEFAULT_SOCKET_TIMEOUT;
         int requestTimeout = ClientConfiguration.DEFAULT_REQUEST_TIMEOUT;
         int clientExecutionTimeout = ClientConfiguration.DEFAULT_CLIENT_EXECUTION_TIMEOUT;
         long connectionTTL = ClientConfiguration.DEFAULT_CONNECTION_TTL;
         long connectionMaxIdleMillis = ClientConfiguration.DEFAULT_CONNECTION_MAX_IDLE_MILLIS;
         int validateAfterInactivityMillis = ClientConfiguration.DEFAULT_VALIDATE_AFTER_INACTIVITY_MILLIS;
         int maxConsecutiveRetriesBeforeThrottling = ClientConfiguration.DEFAULT_MAX_CONSECUTIVE_RETRIES_BEFORE_THROTTLING;
        return new ClientConfiguration()
                .withConnectionTimeout(connectionTimeout)
                .withSocketTimeout(socketTimeout)
                .withClientExecutionTimeout(clientExecutionTimeout)
                .withConnectionMaxIdleMillis(connectionMaxIdleMillis)
                .withMaxConnections(maxConnections)
                .withThrottledRetries(throttleRetries)
                .withRequestTimeout(requestTimeout)
                .withConnectionTTL(connectionTTL)
                .withValidateAfterInactivityMillis(validateAfterInactivityMillis)
                .withMaxConsecutiveRetriesBeforeThrottling(maxConsecutiveRetriesBeforeThrottling);
    }
}
