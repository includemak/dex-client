package com.example.demo;

import com.swiggy.dexp.alchemist.DePerformance;
import com.swiggy.dexp.alchemist.DePerformanceServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.health.v1.HealthCheckRequest;
import io.grpc.health.v1.HealthCheckResponse;
import io.grpc.health.v1.HealthGrpc;
import io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.NettyChannelBuilder;

import javax.net.ssl.SSLException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GrpcClientWithSSL {

    public static void main(String args[]) throws SSLException {
        /*ManagedChannel channel = ManagedChannelBuilder.forAddress("dexpqueryinterface.staging.singapore.swig.gy", 443)
                .build();*/
        ManagedChannel channel = NettyChannelBuilder.forAddress("dexpqueryinterface.staging.singapore.swig.gy", 443)
                .sslContext(GrpcSslContexts.forClient().trustManager(new File("/Users/mayank.prajapati/Downloads/demo 3/src/main/resources/ca.pem")).build())
                .build();
        HealthGrpc.HealthBlockingStub stub = HealthGrpc.newBlockingStub(channel);
        HealthCheckRequest request = HealthCheckRequest.newBuilder().setService("ssdsdf").build();
        HealthCheckResponse response = stub.check(request);
        System.out.println(response.getStatus().name());
        /*DePerformanceServiceGrpc.DePerformanceServiceBlockingStub stub = DePerformanceServiceGrpc.newBlockingStub(channel);
        long start = System.currentTimeMillis();
        long perDETime = System.currentTimeMillis();
        long per1000DETime = System.currentTimeMillis();
        System.out.println("started");
        for(int i=0;i<200000;i++){
            List<DePerformance.PerformanceQuery> performanceQueries = getPerformanceQueries();
            DePerformance.DePerformanceRequest request = DePerformance.DePerformanceRequest.newBuilder()
                    .setDeId(String.valueOf((i%200)))
                    .addAllPerformanceQuery(performanceQueries)
                    .build();
            DePerformance.DePerformanceResponse response = stub.getPerformanceForDe(request);
            System.out.println(i+"th DE, per DE Time"+(System.currentTimeMillis()-perDETime));
            perDETime = System.currentTimeMillis();
            System.out.println(i+"th DE status = "+response.getStatus()+", message= "+response.getMessage());
            if(i%1000 == 0){
                System.out.println("1000 DE time ="+(System.currentTimeMillis()-per1000DETime));
                per1000DETime = System.currentTimeMillis();
            }
        }
        System.out.println("time took ="+(System.currentTimeMillis()-start));*/
        channel.shutdown();
    }
    private static List<DePerformance.PerformanceQuery> getPerformanceQueries() {
        List<DePerformance.PerformanceQuery> queries = new ArrayList<>();
        List<DePerformance.PerformanceQueryMetric> queryMetrics = getQueryMetrics();
        List<DePerformance.PerformanceQueryRange> queryRanges = getQueryRanges();
        DePerformance.PerformanceQuery query = DePerformance.PerformanceQuery.newBuilder()
                .setQueryId("1")
                .addAllPerformanceQueryMetric(queryMetrics)
                .addAllPerformanceQueryRange(queryRanges)
                .build();
        queries.add(query);
        return queries;
    }

    private static List<DePerformance.PerformanceQueryRange> getQueryRanges() {
        List<DePerformance.PerformanceQueryRange> queryRanges = new ArrayList<>();
        {
            DePerformance.PerformanceQueryRange range = DePerformance.PerformanceQueryRange.newBuilder()
                    .setStart("2019-10-06 06:15:00")
                    .setEnd("2019-10-07 05:45:00")
                    .build();
            queryRanges.add(range);
        }
        {
            DePerformance.PerformanceQueryRange range = DePerformance.PerformanceQueryRange.newBuilder()
                    .setStart("2019-10-07 06:15:00")
                    .setEnd("2019-10-08 05:45:00")
                    .build();
            queryRanges.add(range);
        }
        {
            DePerformance.PerformanceQueryRange range = DePerformance.PerformanceQueryRange.newBuilder()
                    .setStart("2019-10-08 06:15:00")
                    .setEnd("2019-10-09 05:45:00")
                    .build();
            queryRanges.add(range);
        }
        {
            DePerformance.PerformanceQueryRange range = DePerformance.PerformanceQueryRange.newBuilder()
                    .setStart("2019-10-09 06:15:00")
                    .setEnd("2019-10-10 05:45:00")
                    .build();
            queryRanges.add(range);
        }
        {
            DePerformance.PerformanceQueryRange range = DePerformance.PerformanceQueryRange.newBuilder()
                    .setStart("2019-10-10 06:15:00")
                    .setEnd("2019-10-11 05:45:00")
                    .build();
            queryRanges.add(range);
        }
        {
            DePerformance.PerformanceQueryRange range = DePerformance.PerformanceQueryRange.newBuilder()
                    .setStart("2019-10-11 06:15:00")
                    .setEnd("2019-10-12 05:45:00")
                    .build();
            queryRanges.add(range);
        }
        {
            DePerformance.PerformanceQueryRange range = DePerformance.PerformanceQueryRange.newBuilder()
                    .setStart("2019-10-12 06:15:00")
                    .setEnd("2019-10-13 05:45:00")
                    .build();
            queryRanges.add(range);
        }
        {
            DePerformance.PerformanceQueryRange range = DePerformance.PerformanceQueryRange.newBuilder()
                    .setStart("2019-10-14 00:00:00")
                    .setEnd("2019-10-15 00:00:00")
                    .build();
            queryRanges.add(range);
        }
        return queryRanges;
    }

    private static List<DePerformance.PerformanceQueryMetric> getQueryMetrics() {
        List<DePerformance.PerformanceQueryMetric> metrics = new ArrayList<>();
        List<String> operands = new ArrayList<>();
        operands.add("delivered_order_count");
        operands.add("delivered_order_count");
        DePerformance.PerformanceQueryMetric avgRating = DePerformance.PerformanceQueryMetric.newBuilder()
                .setName("avg_rating")
                .addAllOperand(operands)
                .setOpType("DERIVATIVE_AVG").build();
        metrics.add(avgRating);
        operands = new ArrayList<>();
        operands.add("delivered_order_count");
        DePerformance.PerformanceQueryMetric sumRating = DePerformance.PerformanceQueryMetric.newBuilder()
                .setName("delivered_order_count")
                .addAllOperand(operands)
                .setOpType("SUM").build();
        metrics.add(sumRating);
        operands = new ArrayList<>();
        operands.add("rejected_order_count");
        DePerformance.PerformanceQueryMetric countRating = DePerformance.PerformanceQueryMetric.newBuilder()
                .setName("rejected_order_count")
                .addAllOperand(operands)
                .setOpType("SUM").build();
        metrics.add(countRating);
        return metrics;
    }
}
