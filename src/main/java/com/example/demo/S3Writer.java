package com.example.demo;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.workdocs.model.DeleteFolderRequest;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.LoggerFactory;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.concurrent.ExecutionException;

public class S3Writer {
    private static Logger log = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);

    public static void main(String args[]) throws IOException {
        log.setLevel(Level.INFO);
        AWSCredentials credentials = new BasicAWSCredentials(
                "AKIAIU2XOBXS7JPLNZGQ",
                "70l8IukxfKsY1KgaJikN2Vj5gsjdVJYAyd4X50or"
        );
        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();

        DateTime start = DateTime.now().withDate(2019,9,21).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter formatter2 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        String baseRawFolder = "swiggy.dex.raw";
        /*int deId = 0;
        for(int i=0;i<2;i++){
            String dayFolder = formatter.print(start);
            System.out.println("day "+dayFolder);
            for(int hour=0;hour <24;hour++){
                String hourFolder = DateTimeFormat.forPattern("HH").print(start);
                System.out.println("hourFolder "+hourFolder);
                for(int min =0; min<60;min++){
                    String minFolder = DateTimeFormat.forPattern("mm").print(start);
                    System.out.println("minFolder "+minFolder);
                    String timeFolder =dayFolder+"/"+hourFolder+"/"+minFolder;
                    //uploadOrderStatusUpdateV2(s3client,baseRawFolder,timeFolder);
                    //uploadOrderIncentiveData(s3client,baseRawFolder,timeFolder);
                    deId = (i*1000)+(hour*100)+(min*10);
                    String loginTime = "2019-09-"+start.getDayOfMonth()+" "+hourFolder+":"+minFolder+":00";
                    String logoutTime = "2019-09-"+start.getDayOfMonth()+" "+hourFolder+":"+minFolder+":59";
                    uploadDeAttendance(deId, loginTime,logoutTime,s3client,baseRawFolder,timeFolder);
                    start = start.plusMinutes(1);
                }
            }
        }*/
        //for (S3ObjectSummary file : s3client.listObjects(baseRawFolder, "de_attendance/2019-11-25").getObjectSummaries()){
          //  s3client.deleteObject(baseRawFolder, file.getKey());
        //}

        java.time.format.DateTimeFormatter ff = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        int deId = 0;
        DateTime dateTime = DateTime.now().withDate(2019,11,25).withHourOfDay(5).withMinuteOfHour(30).withSecondOfMinute(0);
        for(int day = 0;day<2;day++){
            for(int hour=0;hour <24;hour++){
                for(int min =0; min<60;min++){
                    String loginTime = formatter2.print(dateTime);
                    dateTime = dateTime.plusMinutes(1);
                    String logoutTime = formatter2.print(dateTime);
                    deId = (day*1000)+(hour*100)+(min*10);
                    LocalDateTime login = convertToUtc(LocalDateTime.parse(loginTime,ff));
                    LocalDateTime logout = convertToUtc(LocalDateTime.parse(logoutTime,ff));
                    String minFolder = DateTimeFormat.forPattern("mm").print(formatter2.parseDateTime(ff.format(login)));
                    String hourFolder = DateTimeFormat.forPattern("HH").print(formatter2.parseDateTime(ff.format(logout)));
                    String timeFolder =ff.format(login).split(" ")[0]+"/"+hourFolder+"/"+minFolder;
                    uploadDeAttendance(deId, loginTime,logoutTime,s3client,baseRawFolder,timeFolder);
                }
            }
        }
    }


    private static LocalDateTime convertToUtc(LocalDateTime time){
        return time.atZone(ZoneId.systemDefault()).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime();
    }
    private static void uploadDeAttendance(int deId, String loginTime, String logoutTime, AmazonS3 s3client, String baseRawFolder, String timeFolder) throws IOException {
        String eventBucketName = "de_attendance";
        String key ="P-8_1_201910290529_201910290529.182665172";
        String toWrite = "{\"deId\":"+deId+",\"publish_time\":\""+loginTime+"\",\"action\":\"start_duty\"}\n" +
                "{\"deId\":"+deId+",\"publish_time\":\""+logoutTime+"\",\"action\":\"stop_duty\"}";
        System.out.println(timeFolder);
        System.out.println(toWrite);
        File tmpFile = File.createTempFile("test", ".tmp");
        FileWriter writer = new FileWriter(tmpFile);
        writer.write(toWrite);
        writer.close();
        PutObjectRequest req = new PutObjectRequest(baseRawFolder+"/"+eventBucketName+"/"+timeFolder, key, tmpFile);
        s3client.putObject(req);
        tmpFile.deleteOnExit();
    }

    private static void uploadOrderIncentiveData(AmazonS3 s3client, String baseRawFolder, String timeFolder) {
        String eventBucketName = "incentive_order_data";
        String key ="P-8_1_201910290529_201910290529.182665172";
        String jsonFilePath = "/Users/mayank.prajapati/Downloads/demo 3/src/main/resources/dexp_raw/P-8_1_201910290529_201910290529.182665172";
        File file = new File(jsonFilePath);
        PutObjectRequest req = new PutObjectRequest(baseRawFolder+"/"+eventBucketName+"/"+timeFolder, key, file);
        s3client.putObject(req);
    }

    private static void uploadOrderStatusUpdateV2(AmazonS3 s3client, String baseRawFolder, String timeFolder) {
        String eventBucketName = "order_status_update_v2";
        String key ="P-12_1_201910300530_201910300530.69771249";
        String jsonFilePath = "/Users/mayank.prajapati/Downloads/demo 3/src/main/resources/dexp_raw/P-12_1_201910300530_201910300530.69771249";
        File file = new File(jsonFilePath);
        PutObjectRequest req = new PutObjectRequest(baseRawFolder+"/"+eventBucketName+"/"+timeFolder, key, file);
        s3client.putObject(req);
    }
}
