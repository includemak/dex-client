package com.example.demo;

import java.io.*;
import java.net.URL;

public class ConsulWriter {
    public static void main(String args[]) throws FileNotFoundException {
        String projName = "dexp-orchestrator";
        String csvFile = "/Users/mayank.prajapati/Downloads/demo 3/src/main/resources/connsul-staging.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] prop = line.split(cvsSplitBy);
                if(prop.length ==2){
                    //System.out.println("Prop [name= " + prop[0] + " , value=" + prop[1] + "]");
                    //writeToStaging(projName,prop[0], prop[1]);
                    writeToProd(projName,prop[0], prop[1]);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void writeToStaging(String projName, String key, String value) throws IOException, InterruptedException {
        String curl = "curl -X PUT   'http://consul-uat.swiggyops.de/v1/kv/rock-config/staging/"+projName+"/"+key+"?dc=swiggy-aws-uat'   -H 'Postman-Token: dab0ab5a-2fee-42e2-945a-ce491cf7e26a'   -H 'cache-control: no-cache'   -d '\""+value+"\"'";
        System.out.println(curl);

    }

    private static void writeToProd(String projName, String key, String value) throws IOException, InterruptedException {
        String curl = "curl -X PUT   'http://consul.swiggyops.de/v1/kv/rock-config/production/"+projName+"/"+key+"?dc=swiggy-aws'   -H 'Postman-Token: 45b555be-42e3-429d-8b03-5a297b6f87e2'   -H 'X-Consul-Token: 4109e905-ca21-8101-fce2-10f640a9bd15'   -H 'cache-control: no-cache'   -d '\""+value+"\"'";
        System.out.println(curl);

    }
}
