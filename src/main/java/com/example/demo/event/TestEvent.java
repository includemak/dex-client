package com.example.demo.event;

import com.example.demo.event.enums.MonetaEventType;
import com.example.demo.event.factory.EventManagerFactory;
import com.example.demo.event.factory.MonetaEventManager;
import com.example.demo.event.factory.SampleReminderSMSManager;
import com.example.demo.event.model.MonetaEvent;
import com.example.demo.event.model.MonetaEventData;
import com.example.demo.event.model.SampleReminderSMSEventData;

import java.util.ArrayList;
import java.util.List;

public class TestEvent {
    public static void main(String args[]){
        MonetaEventManager manager = new SampleReminderSMSManager();
        List<Long> campaignsIDs = new ArrayList<>();
        campaignsIDs.add(34l);
        campaignsIDs.add(44l);
        MonetaEventData d = new SampleReminderSMSEventData(33l,campaignsIDs);
        //String data = JSONUtils.asJsonString(d);
        MonetaEvent event = new MonetaEvent(MonetaEventType.SAMPLE_REMINDER_SMS.name(),d);
        String eventString = JSONUtils.asJsonString(event);
        System.out.println("eventString = "+eventString);

        MonetaEvent e2 = (MonetaEvent) JSONUtils.jsonDecode(eventString,MonetaEvent.class);
        System.out.println(e2.getEventType());
        System.out.println(e2.getEventData());

        SampleReminderSMSEventData ddd = (SampleReminderSMSEventData)e2.getEventData();
        System.out.println(ddd.getCampaignIds());
    }
}
