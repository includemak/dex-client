package com.example.demo.event.factory;

import com.example.demo.event.model.SampleReminderSMSEventData;
import org.springframework.beans.factory.annotation.Autowired;

public class SampleReminderSMSManager implements MonetaEventManager<SampleReminderSMSEventData> {

    @Override
    public void processEventData(SampleReminderSMSEventData eventData) {

    }
}
