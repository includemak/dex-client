package com.example.demo.event.factory;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan(basePackages = {"com.swiggy.moneta.services.event.factory"})
public class EventManagerFactoryConfig {

    @Bean(name = "eventManagerFactory")
    public ServiceLocatorFactoryBean serviceLocatorBean(){
        ServiceLocatorFactoryBean bean = new ServiceLocatorFactoryBean();
        bean.setServiceLocatorInterface(EventManagerFactory.class);
        return bean;
    }

    @Bean(name = "SAMPLE_REMINDER_SMS")
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public SampleReminderSMSManager sampleReminderSMSManager() {
        return new SampleReminderSMSManager();
    }
}
