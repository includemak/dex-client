package com.example.demo.event.factory;


import com.example.demo.event.enums.MonetaEventType;

public interface EventManagerFactory {
    MonetaEventManager getEventManager(MonetaEventType eventType);
}
