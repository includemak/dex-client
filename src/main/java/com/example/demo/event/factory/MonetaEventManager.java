package com.example.demo.event.factory;


public interface MonetaEventManager<T> {
    void processEventData(T eventData);
}
