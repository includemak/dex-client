package com.example.demo.event;

import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class RuleEngineRuleReader {

    public static void main(String[] args){
        String csvFile = "/Users/mayank.prajapati/Downloads/demo 3/src/main/resources/Rule Config in rule engine - Rule Config.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                // use comma as separator

                String[] prop = line.split(cvsSplitBy);

                if(prop.length >= 7){
                    String attrName = prop[5];
                    String ruleExpr = prop[6];
                    String ruleCode = prop[1];
                    String tenure = prop[2];
                    String ruleName = prop[0];
                    if(!StringUtils.isEmpty(tenure)){
                        System.out.println("\""+ruleCode+"\": map[string]string{\n" +
                                "\t\t\t\"rule_code\":\""+ruleCode+"\",\n" +
                                "\t\t\t\"rule_name\":\""+ruleName+"\",\n" +
                                "\t\t\t\"rule_metadata\":`{\"attributes\":[{\"name\": \""+attrName+"\", \"tenure\": \""+tenure+"\", \"op_type\":\"SUM\", \"operand\":[\""+attrName+"\"]} ], \"rule_expression\": \""+ruleExpr+"\"}`,\n" +
                                "\t\t},");
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
