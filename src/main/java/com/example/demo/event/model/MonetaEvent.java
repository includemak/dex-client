package com.example.demo.event.model;


import com.example.demo.event.enums.MonetaEventType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

public class MonetaEvent {
    @JsonProperty(value = "event_type") private String eventType;
    @JsonProperty(value = "event_data") private MonetaEventData eventData;


    public MonetaEvent(String eventType, MonetaEventData eventData) {
        this.eventType = eventType;
        this.eventData = eventData;
    }
    public MonetaEvent(){
        super();
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public MonetaEventData getEventData() {
        return eventData;
    }

    public void setEventData(MonetaEventData eventData) {
        this.eventData = eventData;
    }
}
