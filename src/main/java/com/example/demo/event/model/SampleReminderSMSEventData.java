package com.example.demo.event.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

public class SampleReminderSMSEventData extends MonetaEventData{
    @JsonProperty(value = "de_id") private Long deId;
    @JsonProperty(value = "campaign_ids") private List<Long> campaignIds;

    public SampleReminderSMSEventData(Long deId, List<Long> campaignIds) {
        this.deId = deId;
        this.campaignIds = campaignIds;
    }
    public SampleReminderSMSEventData(){ super(); }

    public Long getDeId() {
        return deId;
    }

    public List<Long> getCampaignIds() {
        return campaignIds;
    }
}
