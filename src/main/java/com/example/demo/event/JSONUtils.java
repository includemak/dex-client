package com.example.demo.event;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Optional;

@Slf4j
@SuppressWarnings("ALL")
public class JSONUtils {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String asJsonString(final Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static ObjectMapper getObjectMapper(){
        return objectMapper;
    }

    public static Object jsonDecode(String s, Class c) {
        Object u = null;
        if(s!=null) {
            try {
                u = objectMapper.reader(c)
                        .readValue(s);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return u;
    }


    public static <T> String serialize(T object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (IOException var2) {
            throw new IllegalArgumentException("Cannot serialize given object");
        }
    }
    public static <T> T deserialize(String json, Class<T> clazz) {
        try {
            return objectMapper.readValue(json, clazz);
        } catch (IOException var3) {
            throw new IllegalArgumentException("Cannot deserialize given object");
        }
    }

}
