/*
package com.example.demo;

import com.swiggy.dexp.alchemist.DePerformance;
import com.swiggy.dexp.alchemist.DePerformanceServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.ArrayList;
import java.util.List;

public class MainClient {
    public static void main(String args[]){
        ManagedChannel channel = ManagedChannelBuilder.forAddress("https", 8080)
                .usePlaintext()
                .build();
        DePerformanceServiceGrpc.DePerformanceServiceBlockingStub stub = DePerformanceServiceGrpc.newBlockingStub(channel);
        List<DePerformance.PerformanceQuery> performanceQueries = getPerformanceQueries();
        DePerformance.DePerformanceRequest request = DePerformance.DePerformanceRequest.newBuilder()
        .setDeId("614759")
        .addAllPerformanceQuery(performanceQueries)
        .build();

        DePerformance.DePerformanceResponse response = stub.getPerformanceForDe(request);
        System.out.println(response.getStatus());
        System.out.println(response.getMessage());
        System.out.println(response);
        channel.shutdown();
    }
    private static List<DePerformance.PerformanceQuery> getPerformanceQueries() {
        List<DePerformance.PerformanceQuery> queries = new ArrayList<>();
        List<DePerformance.PerformanceQueryMetric> queryMetrics = getQueryMetrics();
        List<DePerformance.PerformanceQueryRange> queryRanges = getQueryRanges();
        DePerformance.PerformanceQuery query = DePerformance.PerformanceQuery.newBuilder()
                .setQueryId("1")
                .addAllPerformanceQueryMetric(queryMetrics)
                .addAllPerformanceQueryRange(queryRanges)
                .build();
        queries.add(query);
        return queries;
    }

    private static List<DePerformance.PerformanceQueryRange> getQueryRanges() {
        List<DePerformance.PerformanceQueryRange> queryRanges = new ArrayList<>();
        DePerformance.PerformanceQueryRange range = DePerformance.PerformanceQueryRange.newBuilder()
                .setStart("2019-09-18 00:00:00")
                .setEnd("2020-09-22 15:15:00")
                .build();
        queryRanges.add(range);
        return queryRanges;
    }

    private static List<DePerformance.PerformanceQueryMetric> getQueryMetrics() {
        List<DePerformance.PerformanceQueryMetric> metrics = new ArrayList<>();
        List<String> operands = new ArrayList<>();
        operands.add("delivered_order_count");
        operands.add("delivered_order_count");
        DePerformance.PerformanceQueryMetric avgRating = DePerformance.PerformanceQueryMetric.newBuilder()
                .setName("avg_rating")
                .addAllOperand(operands)
                .setOpType("DERIVATIVE_AVG").build();
        metrics.add(avgRating);
        operands = new ArrayList<>();
        operands.add("delivered_order_count");
        DePerformance.PerformanceQueryMetric sumRating = DePerformance.PerformanceQueryMetric.newBuilder()
                .setName("delivered_order_count")
                .addAllOperand(operands)
                .setOpType("SUM").build();
        metrics.add(sumRating);
        operands = new ArrayList<>();
        operands.add("reject_order_count");
        DePerformance.PerformanceQueryMetric countRating = DePerformance.PerformanceQueryMetric.newBuilder()
                .setName("reject_order_count")
                .addAllOperand(operands)
                .setOpType("SUM").build();
        metrics.add(countRating);
        return metrics;
    }
}
*/
