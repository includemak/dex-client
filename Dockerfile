FROM java:8
WORKDIR /

COPY build/libs/demo-0.0.1-SNAPSHOT.jar demo-0.0.1-SNAPSHOT.jar
RUN mkdir -p /opt/demo/
RUN mkdir -p /var/log/demo/

CMD java $JAVA_ARGS -jar demo-0.0.1-SNAPSHOT.jar

